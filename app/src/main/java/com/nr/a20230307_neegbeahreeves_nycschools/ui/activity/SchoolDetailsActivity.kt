package com.nr.a20230307_neegbeahreeves_nycschools.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.nr.a20230307_neegbeahreeves_nycschools.R
import com.nr.a20230307_neegbeahreeves_nycschools.databinding.ActivitySchoolDetailsBinding
import com.nr.a20230307_neegbeahreeves_nycschools.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job

@AndroidEntryPoint
class SchoolDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySchoolDetailsBinding
    private lateinit var job: Job
    private val viewModel: SchoolViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        job = Job()

        //  Get Intent of Saved dbn from RecyclerView
        val dataDetails = intent.getStringExtra("dbn")
        dataDetails?.let { viewModel.getSpecifiedSchool(it) }
        binding.schoolTextView.text = intent.getStringExtra("school")

        viewModel.schoolDetails.observe(this) { data ->
            try {
                // Using Nullable just in case some data is missing on API depending on school, it will return the default.
                // Will catch via OutofBoundException and print default
                binding.mathScore.text = data[0]?.sat_math_avg_score
                binding.criticalThinkingScore.text = data[0]?.sat_critical_reading_avg_score
                binding.readingScore.text = data[0]?.sat_writing_avg_score
            } catch (ex: java.lang.IndexOutOfBoundsException){
                binding.mathScore.text = getString(R.string.cannot_determine_score)
                binding.criticalThinkingScore.text = getString(R.string.cannot_determine_score)
                binding.readingScore.text = getString(R.string.cannot_determine_score)
            }
        }

    }
}