package com.nr.a20230307_neegbeahreeves_nycschools.ui.activity.view


import android.content.Context
import android.content.Intent
import androidx.activity.ComponentActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import androidx.core.os.bundleOf
import com.nr.a20230307_neegbeahreeves_nycschools.model.School
import com.nr.a20230307_neegbeahreeves_nycschools.ui.activity.SchoolDetailsActivity


class SchoolCardView(): ComponentActivity() {

    // Card Item
    @Composable
    fun cardItem(school: School){
        val context = LocalContext.current
        Card(
           modifier = Modifier
               .fillMaxWidth().height(75.dp)
               .padding(15.dp)
               .clickable { detailViewOnClick(context, school.dbn, school.school_name) },
           shape = CardDefaults.elevatedShape) {
           Text(
               text = school.school_name,
               style = MaterialTheme.typography.labelLarge,
               modifier = Modifier.padding(start = 16.dp)
           )
       }
    }

    // Recycler View Adapter using LazyColumn
    @Composable
    fun SchoolRecyclerView(schools: List<School>){
        LazyColumn{
            itemsIndexed(items = schools){ index, item ->
                cardItem(school = item)
            }
        }

    }

    private fun detailViewOnClick(
        context: Context,
        data: String,
        schoolData: String
    ){
        val intent = Intent(context, SchoolDetailsActivity::class.java)
        intent.putExtra("dbn", data);
        intent.putExtra("school",schoolData )
        startActivity(context, intent, bundleOf())

    }

    @Preview (name = "School Card", device = Devices.NEXUS_6P, showSystemUi = true)
    @Composable
    fun cardItemPreview(){
        SchoolCardView().SchoolRecyclerView(listOf(School("1","NC A&T SU"), School("2","Howard University")))

    }
}