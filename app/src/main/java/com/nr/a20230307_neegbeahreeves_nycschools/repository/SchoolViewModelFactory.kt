package com.nr.a20230307_neegbeahreeves_nycschools.repository


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nr.a20230307_neegbeahreeves_nycschools.viewmodel.SchoolViewModel

class SchoolViewModelFactory constructor(private val schoolRepository: SchoolRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(SchoolViewModel::class.java)){
            SchoolViewModel(this.schoolRepository) as T
        } else throw IllegalArgumentException("School ViewModel Not Found")
    }
}