package com.nr.a20230307_neegbeahreeves_nycschools.repository

import com.nr.a20230307_neegbeahreeves_nycschools.api.SchoolApiInterface
import javax.inject.Inject


class SchoolRepository @Inject constructor(private val schoolApiInterface: SchoolApiInterface) {

    // Repository to setup to fetch all school and the specific school

    suspend fun getAllSchools() = schoolApiInterface.getAllSchools()

    suspend fun getSpecificSchool(dbn: String) = schoolApiInterface.getSpecificSchool(dbn)
}