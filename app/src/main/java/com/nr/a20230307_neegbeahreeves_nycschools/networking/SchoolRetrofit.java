package com.nr.a20230307_neegbeahreeves_nycschools.networking;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


// Java Version of this instance but better for retrofit to use companion objects. See SchoolApiInterface.java
public class SchoolRetrofit {

    private static Retrofit retrofit;


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl("https://data.cityofnewyork.us")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
