package com.nr.a20230307_neegbeahreeves_nycschools.model

// Just get the school name and dbn
data class School (val dbn: String, val school_name: String)