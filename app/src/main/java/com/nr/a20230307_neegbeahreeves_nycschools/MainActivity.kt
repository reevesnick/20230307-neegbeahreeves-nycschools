package com.nr.a20230307_neegbeahreeves_nycschools

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.nr.a20230307_neegbeahreeves_nycschools.ui.activity.view.SchoolCardView
import com.nr.a20230307_neegbeahreeves_nycschools.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job

@AndroidEntryPoint
class MainActivity() : ComponentActivity() {

    lateinit var job: Job
    private val viewModel: SchoolViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()

        setContent {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {
                IntroText()
                // Populate all data from RecycleView Layout
                viewModel.getAllSchools()
                // Compose way of getting the Observer
                val getData = viewModel.schoolList.observeAsState()
                getData.value?.let { SchoolCardView().SchoolRecyclerView(it) }            }


        }
    }

}

// Intro Test View
@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun IntroText(){
    Column {
        Text(
            text = "Select A NYC School for SAT Info",
            style = MaterialTheme.typography.labelLarge,
            modifier = Modifier.padding(start = 16.dp)
        )
    }
}



// Toolbar to Display. Not displaying by default
@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun MainToolbar() {
    Column {
        TopAppBar(
            title = {
                Text("Select A School")
            }
        )

    }
}
