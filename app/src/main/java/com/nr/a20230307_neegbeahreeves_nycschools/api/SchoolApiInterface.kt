package com.nr.a20230307_neegbeahreeves_nycschools.api

import com.nr.a20230307_neegbeahreeves_nycschools.model.School
import com.nr.a20230307_neegbeahreeves_nycschools.model.SchoolDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface SchoolApiInterface {
    // Get all schools from the list
    @GET ("/resource/s3k6-pzi2.json")
    suspend fun getAllSchools(): Response<List<School>>

    // Specific School based on dbn
    @GET ("/resource/f9bf-2cp4.json")
    suspend fun getSpecificSchool(@Query("dbn") dbn: String): Response<List<SchoolDetails>>

}