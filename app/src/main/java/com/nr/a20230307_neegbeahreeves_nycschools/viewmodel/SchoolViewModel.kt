package com.nr.a20230307_neegbeahreeves_nycschools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nr.a20230307_neegbeahreeves_nycschools.model.School
import com.nr.a20230307_neegbeahreeves_nycschools.model.SchoolDetails
import com.nr.a20230307_neegbeahreeves_nycschools.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val schoolRepository: SchoolRepository): ViewModel() {
    val errorMessage = MutableLiveData<String>() // Error Message Exception
    val schoolList = MutableLiveData<List<School>>()
    val schoolDetails = MutableLiveData<List<SchoolDetails>>()
    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val loading = MutableLiveData<Boolean>()

    init {
        getAllSchools()
        getSpecifiedSchool("0")
    }

    // Pulling Data  with Coroutines from the Main Dispatcher with
    fun getAllSchools(){
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).async {
            val response = schoolRepository.getAllSchools()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    schoolList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    // Get the specified school from the API
    fun getSpecifiedSchool(dbn: String){
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).async {
            val response = schoolRepository.getSpecificSchool(dbn)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    schoolDetails.postValue(response.body())
                    loading.value = false
                }
                else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}
