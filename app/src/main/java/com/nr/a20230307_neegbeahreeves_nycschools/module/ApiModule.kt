package com.nr.a20230307_neegbeahreeves_nycschools.module

import com.nr.a20230307_neegbeahreeves_nycschools.api.SchoolApiInterface
import com.nr.a20230307_neegbeahreeves_nycschools.repository.SchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    // All the modules used to share between class

    // HTTP Interceptor for any detailed request logs
    @Singleton
    @Provides
    fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    // HTTP Client via OkHttp
    @Singleton
    @Provides
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()


    // Retrofit Client
    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://data.cityofnewyork.us")
        .client(okHttpClient)
        .build()

    // API Service for the headers
    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): SchoolApiInterface = retrofit.create(SchoolApiInterface::class.java)

    // Repository
    @Singleton
    @Provides
    fun providesRepository(apiInterface: SchoolApiInterface) = SchoolRepository(apiInterface)
}