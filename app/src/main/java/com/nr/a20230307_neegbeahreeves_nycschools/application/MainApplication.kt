package com.nr.a20230307_neegbeahreeves_nycschools.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication: Application() {
}