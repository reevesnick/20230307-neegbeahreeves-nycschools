package com.nr.a20230307_neegbeahreeves_nycschools.viewModel

import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.nr.a20230307_neegbeahreeves_nycschools.api.SchoolApiInterface
import com.nr.a20230307_neegbeahreeves_nycschools.repository.SchoolRepository
import com.nr.a20230307_neegbeahreeves_nycschools.repository.SchoolViewModelFactory
import com.nr.a20230307_neegbeahreeves_nycschools.viewmodel.SchoolViewModel
import kotlinx.coroutines.Job
import org.junit.After
import org.junit.Before
import org.junit.Test

class ViewModelTests {

    private lateinit var job: Job
    private lateinit var api: SchoolApiInterface
    private lateinit var repo: SchoolRepository
    private lateinit var viewModel: SchoolViewModel

//   val viewModel = ViewModelProvider(this, SchoolViewModelFactory(repo))[SchoolViewModel::class.java]




    @Before
    fun setUp() {

        job = Job()
        api = SchoolApiInterface.getInstance()
        repo = SchoolRepository(api)
        viewModel = SchoolViewModel(repo)
    }

    @Test
    fun `ViewModel - Get All Schools`(){
        viewModel.getAllSchools()

    }


    @After
    fun tearDown(){

    }
}