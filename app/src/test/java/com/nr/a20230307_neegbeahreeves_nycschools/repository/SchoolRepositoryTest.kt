package com.nr.a20230307_neegbeahreeves_nycschools.repository

import com.google.gson.Gson
import com.nr.a20230307_neegbeahreeves_nycschools.api.SchoolApiInterface
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SchoolRepositoryTest {

    private lateinit var repository: SchoolRepository
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var api: SchoolApiInterface
    private lateinit var mockWebServer: MockWebServer
    private lateinit var gson: Gson

    @Before
    fun setUp(){
        // Creates the MockWebServer
        mockWebServer = MockWebServer()

        // Creates OkHttpClient
        okHttpClient = OkHttpClient.Builder()
            .writeTimeout(1, TimeUnit.SECONDS)
            .readTimeout(1, TimeUnit.SECONDS)
            .connectTimeout(1, TimeUnit.SECONDS)
            .build()

        // Creates Retrofit
        api = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(mockWebServer.url("https://data.cityofnewyork.us"))
            .build()
            .create(SchoolApiInterface::class.java)

        // Creates the repository
        repository = SchoolRepository(api)
    }

    @Test
    fun `Get All Data - Is Response is OK`() = runBlocking {

        // Schedule the response
        mockWebServer.enqueue(MockResponse().setResponseCode(200))
        val result = repository.getAllSchools()
        assertTrue(result.isSuccessful)
    }

    @Test
    fun `Get All Data - Is Response Has Internal Server Error`() = runBlocking {

        mockWebServer.enqueue(MockResponse().setResponseCode(500))
        val result = repository.getAllSchools()
        assertTrue(result.code() == 500)
    }

    @Test
    fun `Get All Data - Is Response Has Auth Error`() = runBlocking {

        mockWebServer.enqueue(MockResponse().setResponseCode(400))
        val result = repository.getAllSchools()
        assertTrue(result.code() == 400)
    }



    @Test
    fun `Get Specific Data - Is Data is OK`() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(200))
        val result = repository.getSpecificSchool("100")
        assertTrue(result.isSuccessful)
    }

    @Test
    fun `Get Specific Data - Is Data is Has Internal Server Error`() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(500))
        val result = repository.getSpecificSchool("100")
        assertTrue(result.code() == 500)
    }

    @Test
    fun `Get Specific Data - Is Data is Has Auth Error`() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(400))
        val result = repository.getSpecificSchool("100")
        assertTrue(result.code() == 400)
    }

    @After
    fun tearDown() {
        // Shuts down the server
        mockWebServer.shutdown()
    }
}