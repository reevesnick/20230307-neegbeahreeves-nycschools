package com.nr.a20230307_neegbeahreeves_nycschools.model

import org.junit.Assert
import org.junit.Test

class SchoolModelTest {


    @Test
    fun `School - Validation`(){
        val school = School("2","New York School")
        Assert.assertNotNull(school)

    }

    @Test
    fun `School - Validation Missing Data`(){
        val school = School("","New York School")
        Assert.assertNotNull(school)

    }

    @Test
    fun `School Details - Validation Missing Data`(){
        val school = SchoolDetails("","New York School","5","450","", "350")
        Assert.assertNull(school)

    }
}